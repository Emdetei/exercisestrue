"""
Задание 5.3

В прошлом задании для вывода на экран данных о каждом объекте мы писали отдельный код 
        для каждого объекта. Это громоздко и нерационально, ООП позволяет оптимизировать код.
В классе Contact создайте метод show_contact(), который будет выводить данные 
    любого объекта типа Contact в том же виде, как сейчас их выводит функция print_contact.
В теле класса Contact напишите метод show_contact, который в качестве параметра 
    будет принимать переменную self. В теле метода выполните print(), точно такой же, 
    как в функции print_contact, только вместо имени объекта в аргументе укажите self.
Вызовите метод show_contact для объектов mike и vlad
Удалите из кода функцию print_contact().
Запустите код

"""


class Contact:
    def __init__(self, name, phone, birthday, address):
        self.name = name
        self.phone = phone
        self.birthday = birthday
        self.address = address
        print(f"Создаём новый контакт {name}")
    
    # здесь напишите метод show_contact()
    # он будет очень похож на функцию print_contact()


mike = Contact("Михаил Булгаков", "2-03-27", "15.05.1891", "Россия, Москва, Большая Пироговская, дом 35б, кв. 6")
vlad = Contact("Владимир Маяковский", "73-88", "19.07.1893", "Россия, Москва, Лубянский проезд, д. 3, кв. 12")


def print_contact():
    print(f"{mike.name} — адрес: {mike.address}, телефон: {mike.phone}, день рождения: {mike.birthday}")
    print(f"{vlad.name} — адрес: {vlad.address}, телефон: {vlad.phone}, день рождения: {vlad.birthday}")

# обратитесь к методу show_contact() объекта mike 
# и к методу show_contact() объекта vlad